# Doghero Test

## Considerações

Tive algumas dúvidas durante a interpretação do teste e considerei as seguintes premissas:

* A unidade de tempo é segundos;
    * 1800 seg = 30 min;
    * 3600 seg = 60 min;
* A unidade de dinheiro é centavos;
    * 2500 cent = R$ 25.00;
    * 3500 cent = R$ 35.00;
* Não entendi por que a API de criação do passeio recebe o:
    * Atributo de preço, pois ele é calculado automaticamente. Com isso, considerei removê-lo da API de criação;
    * Horário de início do passeio, pois há uma rota para iniciar o passeio. Com isso, considerei removê-lo da API de criação;
    * Horário de fim do passeio, pois há uma rota para finalizar o passeio. Com isso, considerei removê-lo da API de criação;
* Os status do passeio são: created, started e finished;

## Mudanças 

Alterei a proposta do teste em relação as rotas para ficar mais semântico:

* [POST] /api/v1/dog_walkings
    * Criar o passeio;

```json
{
	"scheduling_date" : "2018-08-02T00:44:57.152Z",
	"pets" : 2,
	"duration": 3600,
	"latitude": "-23.591046",
	"longitude": "-46.6428306"
}
```

* [GET] /api/v1/dog_walkings?page=X&quantity=X&flag=next
    * Apresenta lista com paginação de todos os itens ou;
    * Com a flag `next` para apresentar os próximos passeios a partir de hoje;
* [GET] /api/v1/dog_walkings/:id - Consulta o passeio;
* [GET] /api/v1/dog_walkings/:id/start - Inicia o passeio;
* [GET] /api/v1/dog_walkings/:id/finish - Finaliza o passeio;
* [GET] /api/v1/dog_walkings/:id/duration - Duração total do passeio em segundos;


## Executar localmente

1. Crie o banco de dados postgres;
2. Execute os comandos na seguinte ordem:

```bash
rails db:create
rails db:migrate
```

3. Inicie o servidor

```bash
rails s
```

## Contatos

* Cândido Sales Gomes - [@candidosales](https://twitter.com/candidosales)
