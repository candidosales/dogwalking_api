# frozen_string_literal: true

module Response
  def json_response(object, meta = {}, status = :ok)
    render json: object, meta: meta, status: status
  end
end
