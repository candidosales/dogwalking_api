# frozen_string_literal: true

class Api::V1::DogWalkingsController < ApplicationController
  before_action :set_param_page, only: [:index]
  before_action :set_dog_walking, only: %i[show update destroy start_walk finish_walk duration]

  def index
    @dog_walkings = DogWalking.paginate(page: params[:page], per_page: params[:quantity] || 10)
    @dog_walkings = @dog_walkings.where('scheduling_date >= ?', DateTime.now) if params[:flag].present? && params[:flag] == 'next'
    meta = pagination_dict(@dog_walkings)
    json_response(@dog_walkings, meta)
  end

  def create
    @dog_walking = DogWalking.new(dog_walking_params)
    if @dog_walking.save
      json_response(@dog_walking, :created)
    else
      json_response(@dog_walking.errors, :unprocessable_entity)
    end
  end

  def show
    json_response(@dog_walking)
  end

  def duration
    json_response({ duration_real: @dog_walking.duration_real })
  end

  def start_walk
    unless @dog_walking.start_time.blank?
      return json_response({ status: 'ERROR', message: 'This walking has already been started' }, :unprocessable_entity)
    end

    @dog_walking.update_attributes(status: 'started', start_time: DateTime.now)
    json_response(@dog_walking, :ok)
  end

  def finish_walk
    if @dog_walking.start_time.blank?
      return json_response({ status: 'ERROR', message: 'this walking has not been started' }, :unprocessable_entity)
    end

    unless @dog_walking.end_time.blank?
      return json_response({ status: 'ERROR', message: 'This walking has already been finished' }, :unprocessable_entity)
    end

    @dog_walking.update_attributes(status: 'finished', end_time: DateTime.now)
    json_response(@dog_walking, :ok)
  end

  private

  def set_dog_walking
    @dog_walking = DogWalking.find(params[:id])
  end

  def dog_walking_params
    params.permit(
      :scheduling_date,
      :pets,
      :duration,
      :latitude,
      :longitude,
      :start_time,
      :end_time
    )
  end
end
