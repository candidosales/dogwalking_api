# frozen_string_literal: true

class ApplicationController < ActionController::API
    include Response
    include ExceptionHandler

    def pagination_dict(collection)
        {
          current_page: collection.current_page,
          next_page: collection.next_page,
          prev_page: collection.previous_page || 0,
          total_pages: collection.total_pages,
          total_count: collection.total_entries
        }
    end

    def set_param_page
        params[:page] = params[:page].to_i + 1 if params[:page]
    end
end
