class DogWalkingSerializer < ActiveModel::Serializer
    attributes :id, :status, :scheduling_date, :price, :duration, :pets, :latitude, :longitude, :start_time, :end_time
end