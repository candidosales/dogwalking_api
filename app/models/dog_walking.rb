# frozen_string_literal: true

class DogWalking < ApplicationRecord
  include Calcule

  before_save :calculate_price
  before_save :define_status

  default_scope { order(created_at: :desc) }

  validates :scheduling_date, :pets, :duration, :latitude, :longitude, presence: true
  validates :pets, numericality: { greater_than: 0 }
  validates :duration, numericality: { greater_than: 0 }
  validates :latitude, numericality: true
  validates :longitude, numericality: true
  validate :validate_duration

  def duration_real
    (end_time - start_time).seconds if (end_time && start_time)
  rescue 
    0
  end

  protected

  def validate_duration
    unless duration.in? [1800, 3600]
      errors.add(:duration, 'duration should be 1800 or 3600 seconds')
    end
  end

  def calculate_price
    self.price = price_walk(pets, duration)
  end

  def define_status
    self.status = 'created'
  end
end
