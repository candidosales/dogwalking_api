# frozen_string_literal: true

module Calcule

  CONFIG_PRICE = {
    1800 => {
      price: 2500,
      add_dog: 1500
    },
    3600 => {
      price: 3500,
      add_dog: 2000
    }
  }

  def price_walk(pets, period_seconds)
    calculate_by_seconds(pets, CONFIG_PRICE[period_seconds])
  rescue
    0
  end

  def calculate_by_seconds(pets, walk_seconds)
    ((pets - 1) * walk_seconds[:add_dog]) + walk_seconds[:price]
  end
end
