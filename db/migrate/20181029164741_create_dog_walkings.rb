class CreateDogWalkings < ActiveRecord::Migration[5.2]
  def change
    create_table :dog_walkings, id: :uuid do |t|
      t.string :status
      t.date :scheduling_date
      t.integer :price
      t.integer :duration
      t.integer :pets
      t.float :latitude
      t.float :longitude
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
