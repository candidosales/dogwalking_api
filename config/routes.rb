# frozen_string_literal: true

Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :dog_walkings

			get 'dog_walkings/:flag', to: 'dog_walkings#index'
      get 'dog_walkings/:id/start', to: 'dog_walkings#start_walk'
      get 'dog_walkings/:id/finish', to: 'dog_walkings#finish_walk'
      get 'dog_walkings/:id/duration', to: 'dog_walkings#duration'
    end
  end
end
